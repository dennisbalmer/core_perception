#!/usr/bin/env python3

import pygeodesy
import tf
import sys


if len(sys.argv) < 3:
  print("Provide the latitude and longitude")
  exit(1)
elif len(sys.argv) == 3:
  lat = sys.argv[1]
  lon = sys.argv[2]
  hgt = 0
elif len(sys.argv) == 4:
  lat = sys.argv[1]
  lon = sys.argv[2]
  hgt = sys.argv[3]


karney = pygeodesy.ecef.EcefKarney(pygeodesy.ellipsoids.Ellipsoids.WGS84)
ecef = karney.forward(lat, lon, hgt, M=True)

M = ecef[7]
R = tf.transformations.random_rotation_matrix()

R[0, 0] = M[0]
R[0, 1] = M[1]
R[0, 2] = M[2]
R[1, 0] = M[3]
R[1, 1] = M[4]
R[1, 2] = M[5]
R[2, 0] = M[6]
R[2, 1] = M[7]
R[2, 2] = M[8]

q = tf.transformations.quaternion_from_matrix(R)

print("ROS transform for the ENU frame located at ({0}, {1}, {2}):".format(lat, lon, hgt))
print(f"{ecef[0]:.10f} {ecef[1]:.10f} {ecef[2]:.10f}")
print(f"{q[0]:.10f} {q[1]:.10f} {q[2]:.10f} {q[3]:.10f}")
