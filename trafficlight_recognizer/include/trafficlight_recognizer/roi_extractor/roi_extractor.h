/*
 * Copyright 2019 Autoware Foundation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TRAFFICLIGHT_RECOGNIZER_ROI_EXTRACTOR_ROI_EXTRACTOR_H
#define TRAFFICLIGHT_RECOGNIZER_ROI_EXTRACTOR_ROI_EXTRACTOR_H

#include <string>

#include <ros/ros.h>
#include <autoware_msgs/Signals.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>

#include <opencv2/opencv.hpp>

class ROIExtractor
{
public:
  ROIExtractor();

  void GetROSParam();
  void StartSubscribersAndPublishers();
  void ImageRawCallback(const sensor_msgs::Image& image);
  void ROISignalCallback(const autoware_msgs::Signals::ConstPtr& extracted_pos);
  // Utility function to create directory which roi images will be saved
  void CreateTargetDirectory();

private:
  // Utility function to count the number of files contained in the specified directory
  int CountFileNum(const std::string& directory_name);
  // Utility function to create directory tree
  void MakeDirectoryTree(const std::string& target, const mode_t& mode);
  // The function to calculate similarity of two image
  double CalculateSimilarity(const cv::Mat& image1, const cv::Mat& image2);
  // The function to write text on frame_
  void PutDebugText(const std::string& text);

  // Subscribers
  ros::Publisher roi_image_publisher_;
  // Publishers
  ros::Subscriber image_subscriber_;
  ros::Subscriber roi_signal_subscriber_;

  // Source image topic name that will be extracted
  std::string image_topic_name_;
  // Directory path that extracted ROI images will be saved
  std::string target_directory_;
  // The minimum height threshold of ROI image
  int minimum_roi_height_;
  // The threshold of the level of similarity
  double similarity_threshold_;
  // flag to save extracted roi image locally
  bool save_roi_image_;

  // The frame image ptr
  cv_bridge::CvImagePtr frame_;
  // The image saved last time
  cv::Mat previous_saved_frame_;
  // Time stamp value of subscribed frame
  ros::Time current_frame_timestamp_;
  // Time stamp value of previously subscribed frame
  ros::Time previous_frame_timestamp_;

  // The number of files contained in the target directory
  int file_count_;
};

#endif  // TRAFFICLIGHT_RECOGNIZER_ROI_EXTRACTOR_ROI_EXTRACTOR_H
