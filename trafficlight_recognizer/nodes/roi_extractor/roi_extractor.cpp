/*
 * Copyright 2019 Autoware Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "trafficlight_recognizer/roi_extractor/roi_extractor.h"
#include "trafficlight_recognizer/context.h"

#include <sys/stat.h>
#include <dirent.h>
#include <stdlib.h>
#include <string>
#include <utility>
#include <vector>

#include <opencv2/opencv.hpp>

#include <ros/ros.h>
#include <autoware_msgs/Signals.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>

ROIExtractor::ROIExtractor() :
  image_topic_name_("/image_raw"),
  target_directory_(std::string(getenv("HOME")) + "/autoware.ai"),
  minimum_roi_height_(32),
  similarity_threshold_(0.9),
  save_roi_image_(false),
  previous_saved_frame_(cv::Mat())
{
}

void ROIExtractor::GetROSParam()
{
  ros::NodeHandle pnh("~");

  pnh.param<std::string>("image_raw_topic", image_topic_name_, "/image_raw");
  ROS_INFO("image_raw_topic: %s", image_topic_name_.c_str());

  pnh.param<std::string>("target_directory", target_directory_,
                                          std::string(getenv("HOME")) + "/autoware.ai");
  ROS_INFO("target_directory: %s", target_directory_.c_str());

  pnh.param<int>("minimum_roi_height", minimum_roi_height_, 32);
  ROS_INFO("minimum_roi_height: %d", minimum_roi_height_);

  pnh.param<double>("similarity_threshold", similarity_threshold_, 0.9);
  ROS_INFO("similarity_threshold: %f", similarity_threshold_);

  pnh.param<bool>("save_roi_image", save_roi_image_, false);
  ROS_INFO("save_roi_image: %s", save_roi_image_ ? "true" : "false");
}

void ROIExtractor::StartSubscribersAndPublishers()
{
  // Launch callback function to subscribe images and signal position
  ros::NodeHandle node_handler;
  roi_image_publisher_ = node_handler.advertise<sensor_msgs::Image>("tlr_roi_image", 1);
  image_subscriber_ = node_handler.subscribe(image_topic_name_, 1, &ROIExtractor::ImageRawCallback, this);
  roi_signal_subscriber_ = node_handler.subscribe("/roi_signal", 1, &ROIExtractor::ROISignalCallback, this);
}

void ROIExtractor::ImageRawCallback(const sensor_msgs::Image& image)
{
  // Acquire frame image from ros topic
  frame_ = cv_bridge::toCvCopy(image, sensor_msgs::image_encodings::BGR8);
  // Save this topic's time stamp so that same image will not be processed more than twice
  current_frame_timestamp_ = image.header.stamp;
}  // void ROIExtractor::ImageRawCallback()

void ROIExtractor::PutDebugText(const std::string& text)
{
  static int y_offset = 50;
  // "[0]" represents start of new set of signals. Add to offset for a new line otherwise
  y_offset = (text.find("[0]") != std::string::npos) ? 50 : (y_offset + 25);
  cv::putText(frame_->image,            // Image
              text,                     // Text
              cv::Point(20, y_offset),  // Coordinates
              cv::FONT_HERSHEY_PLAIN,   // Font
              1.5,                      // Scale
              cv::Scalar(0, 0, 0),      // BGR Color
              2);                       // Thickness
  return;
}

void ROIExtractor::ROISignalCallback(const autoware_msgs::Signals::ConstPtr& extracted_pos)
{
  // If frame image has not been updated, do nothing
  if (!frame_ || current_frame_timestamp_ == previous_frame_timestamp_)
  {
    return;
  }

  // Aquire signal positions from ros topic
  std::vector<Context> signal_positions;
  Context::SetContexts(&signal_positions, extracted_pos, frame_->image.rows, frame_->image.cols);

  if (signal_positions.size() == 0)
  {
    // If signal_positions is empty, no ROI images should be saved
    return;
  }

  // Extract ROI for top signal in vector (top signal has largest estimated radius in every signals projected in a
  // image)
  cv::Mat roi = frame_->image(cv::Rect(signal_positions.at(0).topLeft, signal_positions.at(0).botRight));

  if (save_roi_image_ &&
      // Reject ROI image if its height is smaller than threshold
      roi.size().height >= minimum_roi_height_ &&
      // Reject ROI image if its similarity level with previous saved ROI is higher than threshold
      CalculateSimilarity(roi, previous_saved_frame_) <= similarity_threshold_)
  {
    std::string file_name = target_directory_ + std::to_string(file_count_) + ".png";
    cv::imwrite(file_name.c_str(), roi);
    ROS_INFO("Saved: %s", file_name.c_str());
    previous_saved_frame_ = std::move(roi);
    file_count_++;
  }

  for (size_t i = 0; i < signal_positions.size(); i++)
  {
    cv::rectangle(frame_->image,
                  signal_positions.at(i).topLeft,
                  signal_positions.at(i).botRight,
                  cv::Scalar(0, 0, 0), 2);

    PutDebugText("[" + std::to_string(i) + "] ");
    PutDebugText("ID: " + std::to_string(signal_positions.at(i).signalID));
    PutDebugText("redCenter2d: (" + std::to_string(signal_positions.at(i).redCenter.x) +
                            " , " + std::to_string(signal_positions.at(i).redCenter.y) + ")");
    PutDebugText("redCenter3d: (" + std::to_string(signal_positions.at(i).redCenter3d.x) +
                            " , " + std::to_string(signal_positions.at(i).redCenter3d.y) +
                            " , " + std::to_string(signal_positions.at(i).redCenter3d.z) + ")");
    PutDebugText("lampRadius: " + std::to_string(signal_positions.at(i).lampRadius));
    PutDebugText("TurnSignal: [L = " + std::to_string(signal_positions.at(i).leftTurnSignal) +
                           "] [R = " + std::to_string(signal_positions.at(i).rightTurnSignal) + "]");
    PutDebugText("closestLaneId: " + std::to_string(signal_positions.at(i).closestLaneId));

    // label each roi with i on the image
    cv::putText(frame_->image, std::to_string(i),
              cv::Point((signal_positions.at(i).topLeft.x + signal_positions.at(i).botRight.x) / 2,
                        (signal_positions.at(i).topLeft.y + signal_positions.at(i).botRight.y) / 2),
              cv::FONT_HERSHEY_SIMPLEX, 1.0, cv::Scalar(0, 0, 0), 2.5);
  }

  roi_image_publisher_.publish(frame_->toImageMsg());
  previous_frame_timestamp_ = current_frame_timestamp_;
  frame_.reset();
}  // void ROIExtractor::ROISignalCallback()

void ROIExtractor::CreateTargetDirectory()
{
  if (!save_roi_image_)
    return;

  struct stat directory_info;
  if (stat(target_directory_.c_str(), &directory_info) != 0)
  {
    target_directory_ = std::string(getenv("HOME")) + "/autoware.ai";
  }
  // Extracted ROI's images will be saved in "[base_name]/tlr_TrainingDataSet/Images"
  target_directory_ = target_directory_ + "/tlr_TrainingDataSet/Images/";
  ROS_INFO("Target Directory: %s", target_directory_.c_str());

  // Create target directory newly if it doesn't exist
  if (stat(target_directory_.c_str(), &directory_info) != 0)
  {
    MakeDirectoryTree(target_directory_, S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);  // 0755
  }

  // Count the number of files contained in the target directory
  // so that saved file is named in continuous number
  file_count_ = CountFileNum(target_directory_);
}  // void ROIExtractor::CreateTargetDirectory

int ROIExtractor::CountFileNum(const std::string& directory_name)
{
  int file_num = 0;
  struct dirent* entry;
  DIR* directory_handler = opendir(directory_name.c_str());

  // Count the number of files contained in the specified directory
  while ((entry = readdir(directory_handler)) != NULL)
  {
    struct stat status;
    std::string absolute_path = directory_name + std::string(entry->d_name);
    if (stat(absolute_path.c_str(), &status) == 0 && S_ISREG(status.st_mode))
    {
      file_num++;
    }
  }

  closedir(directory_handler);

  return file_num;
}  // int ROIExtractor::CountFileNum()

void ROIExtractor::MakeDirectoryTree(const std::string& target, const mode_t& mode)
{
  // Extract directory subtree structure
  std::string path = std::string(getenv("HOME"));
  std::string sub_tree = target.substr(path.size());

  // Create directory tree one by one
  size_t separator_start = sub_tree.find("/");
  size_t separator_end = sub_tree.find("/", separator_start + 1);
  while (separator_end != std::string::npos)
  {
    std::string sub_directory = sub_tree.substr(separator_start, separator_end - separator_start);
    path = path + sub_directory;
    mkdir(path.c_str(), mode);
    separator_start = separator_end;
    separator_end = sub_tree.find("/", separator_start + 1);
  }
}  // void ROIExtractor::MakeDirectoryTree()

// calculae similarity of specified two images
// by comparing their histogram, which is sensitive filter for color
double ROIExtractor::CalculateSimilarity(const cv::Mat& image1, const cv::Mat& image2)
{
  if (image1.empty() || image2.empty())
  {
    return 0.0;
  }

  // Compare by histogram
  cv::Mat image1_hsv, image2_hsv;
  cv::cvtColor(image1, image1_hsv, CV_BGR2HSV);
  cv::cvtColor(image2, image2_hsv, CV_BGR2HSV);

  const int channel[] = { 0 };

  // Hue range in OpenCV is 0 to 180
  const float hue_ranges[] = { 0, 180 };
  const float* ranges[] = { hue_ranges };

  // Quantize hue value into 6
  int hist_size[] = { 6 };

  cv::Mat histogram1;
  cv::calcHist(&image1_hsv,
               1,  // Use this image only to create histogram
               channel,
               cv::Mat(),  // No mask is used
               histogram1,
               1,  // The dimension of histogram is 1
               hist_size, ranges);

  cv::Mat histogram2;
  cv::calcHist(&image2_hsv,
               1,  // Use this image only to create histogram
               channel,
               cv::Mat(),  // No mask is used
               histogram2,
               1,  // The dimension of histogram is 1
               hist_size, ranges);

  double similarity = cv::compareHist(histogram1, histogram2, CV_COMP_CORREL);

  return similarity;
}  // void ROIExtractor::CalculateSimilarity()

// Entry Point of this node
int main(int argc, char* argv[])
{
  // Initialize ROS node
  ros::init(argc, argv, "roi_extractor");

  // Get directory name which roi images will be saved
  ROIExtractor extractor;
  extractor.GetROSParam();
  extractor.CreateTargetDirectory();
  extractor.StartSubscribersAndPublishers();

  ros::spin();

  return 0;
}
